package com.infharo;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

public class Programmer{
    public void run(){
        try {
            JobDetail job1 = JobBuilder.newJob(Job1.class).withIdentity("job1", "group1").build();
            JobDetail job2 = JobBuilder.newJob(Job2.class).withIdentity("job2", "group2").build();

            Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("myTrigger1","group1")
                .startNow()
                .withPriority(1)
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                    .withIntervalInSeconds(5)
                    .repeatForever())
                .build();
            
            Trigger trigger2 = TriggerBuilder.newTrigger()
                .withIdentity("myTrigger2","group2")
                .startNow()
                .withPriority(2)
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                    .withIntervalInSeconds(10)
                    .repeatForever())
                .build();
    
            SchedulerFactory sf = new StdSchedulerFactory();
            Scheduler sched = sf.getScheduler();
            sched.scheduleJob(job1, trigger);
            sched.scheduleJob(job2, trigger2);
            sched.start();
        } catch (SchedulerException se) {
            se.printStackTrace();
        }
    }
}